//Programul receptor
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/types.h> // pt tipul pid_t
#include<signal.h>	// pt semnale

int nr=0, value; 
pid_t pa;
sigset_t ms;	// o masca pt adormirea procesului pana la primirea unui semnal neblocat de ms

void h1(int n){
	signal(n,h1); 
	kill(pa,SIGUSR1);	//trimite lui b confirmare ca poate trimite semnal
}

void h2(int n){
	printf("Am primit: %d\n",nr); 
	exit(0);
}

int main(){
	signal(SIGUSR1,h1);
	signal(SIGINT,h2);
	sigemptyset(&ms);
	sigaddset(&ms, SIGUSR1);
	sigprocmask(SIG_SETMASK, &ms, NULL);

 	printf("PID propriu: %d\n",getpid());
	printf("PID advers: "); scanf("%d",&pa);

	if(pa == getpid()){
		printf("Nu puteti introduce PID-ul acestui proces!\n");
		return 1;
	}

	value = kill(pa, 0);	//intoarce -1 daca nu exista procesul pa

	if(value == -1) {
		printf("PID inexistent!\n");
		return 1;
	}

	sigfillset(&ms);
	sigdelset(&ms, SIGUSR1);
	sigdelset(&ms, SIGINT);

 	while(1){
		sigsuspend(&ms);
		++nr;
	}
	
 	return 0;
}
