// Programul emitator:
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/types.h> // pt tipul pid_t
#include<signal.h>	// pt semnale

int nr=0, value;
pid_t pa;
sigset_t ms;

void h1(int n){
	signal(n,h1);
}

void h2(int n){
	printf("Am trimis: %d\n",nr); 
	exit(0);
}

int main() {
	int i;
	signal(SIGUSR1,h1);
	signal(SIGINT, h2);

	sigemptyset(&ms);
	sigaddset(&ms, SIGUSR1);
	sigprocmask(SIG_SETMASK, &ms, NULL);

	printf("PID propriu: %d\n", getpid());
	printf("PID advers: "); scanf("%d", &pa);

	if(pa == getpid()){
		printf("Nu puteti introduce PID-ul acestui proces!\n");
		return 1;
	}
	value = kill(pa, 0);

	if(value == -1){
		printf("PID inexistent!\n");
	return 1;
	}

	sigfillset(&ms);
	sigdelset(&ms, SIGUSR1);

	for(i=0;i<2000;++i) {
		value = kill(pa,SIGUSR1);	//trimite semnalul SIGUSR1
		if(value == -1) {
				perror("Eroare la trimiterea semnalului");
				return 1;
		}
		++nr;
		sigsuspend(&ms);
	}
	printf("Am trimis: %d\n",nr);
	return 0;
}
